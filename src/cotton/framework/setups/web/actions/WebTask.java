package cotton.framework.setups.web.actions;

import cotton.framework.actions.Action;
import cotton.framework.core.Client;
import cotton.framework.core.Session;
import cotton.framework.services.BrowserService;
import cotton.framework.services.ServiceSupplier;

public abstract class WebTask<I extends WebTask.Input, O extends Action.Output> implements Action.Task<I, O> {
	protected final ServiceSupplier serviceSupplier;

	public WebTask(ServiceSupplier serviceSupplier) {
		this.serviceSupplier = serviceSupplier;
	}

	@Override
	public void before(I input, O output) {
		linkTask(clientOf(input));
	}

	@Override
	public void after(I input, O output) {
		unlinkTask();
	}

	private void linkTask(Client client) {
		if (client != null)
			browserService().linkToThread(client);
	}

	private void unlinkTask() {
		browserService().unlinkFromThread();
	}

	protected <T extends Client> T clientOf(Input input) {
		return (T) clientOf(input.clientId());
	}

	protected <T extends Client> T clientOf(String id) {
		return serviceSupplier.service(BrowserService.class).getClient(id);
	}

	protected <T extends Session> T sessionOf(Input input) {
		return (T) sessionOf(input.sessionId());
	}

	protected <T extends Session> T sessionOf(String id) {
		return serviceSupplier.service(BrowserService.class).getSession(id);
	}

	protected BrowserService browserService() {
		return serviceSupplier.service(BrowserService.class);
	}

	public interface Input extends Action.Input {
		String displayId();
		String operation();
	}
}
