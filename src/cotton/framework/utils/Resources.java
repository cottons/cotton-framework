package cotton.framework.utils;

import java.io.InputStream;

public class Resources {

	public static InputStream getAsStream(String name) {
		return Resources.class.getResourceAsStream(name);
	}

	public static boolean exists(String name) {
		return Resources.class.getResource(name) != null;
	}

	public static String getFullPath(String name) {
        String directory = Resources.class.getResource(name).getPath();

        if (directory.charAt(directory.length() - 1) == '/')
            return directory.substring(0, directory.length() - 1);

        return directory;
	}

}
