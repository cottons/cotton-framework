package cotton.framework.setups;

import cotton.framework.actions.*;
import cotton.framework.services.PushService;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public abstract class CottonServer implements AdapterProxy {
	private final Map<String, RequestAdapter> requestAdapters = new HashMap<>();
	private final Map<String, ResponseAdapter> responseAdapters = new HashMap<>();
	private final String appName;

	public CottonServer(String appName) {
		this.appName = appName;
	}

	public void register(PushService pushService) {
		router().pushService(routeOf("/push"), pushService);
	}

	public Router.Routing route(String path) {
		return router().route(routeOf(path));
	}

	public RegisterRequestAdapterTask adaptRequest(String... methods) {
		return adapter -> register(methods, method -> requestAdapters.put(method, adapter));
	}

	public RegisterResponseAdapterTask adaptResponse(String... methods) {
		return adapter -> register(methods, method -> responseAdapters.put(method, adapter));
	}

	private String routeOf(String path) {
		return "/" + appName + path;
	}

	private void register(String[] methods, Consumer<String> consumer) {
		Stream.of(methods).forEach(consumer::accept);
	}

	protected abstract Router router();

	@SuppressWarnings("unchecked")
	@Override
	public RequestAdapter requestAdapterOf(String name) {
		return requestAdapters.getOrDefault(name, new DefaultRequestAdapter());
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseAdapter responseAdapterOf(String name) {
		return responseAdapters.getOrDefault(name, new DefaultResponseAdapter());
	}

	@FunctionalInterface
	public interface RegisterRequestAdapterTask {
		void with(RequestAdapter adapter);
	}

	@FunctionalInterface
	public interface RegisterResponseAdapterTask {
		void with(ResponseAdapter adapter);
	}
}