package cotton.framework.displays;

public interface International {
    void onChangeLanguage(String newLanguage);
}
