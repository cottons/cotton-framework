package cotton.framework.setups.web.actions;

import cotton.framework.actions.RequestAdapter;
import spark.Request;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.util.Locale;
import java.util.Scanner;

class RequestWrapper extends SparkWrapper {

	private static final String SessionId = "sessionId";
	private static final String Request = "request";
	private static final String RequestUrl = "requestUrl";
	private static final String LanguageFromUrl = "languageFromUrl";
	private static final String LanguageFromHeader = "languageFromHeader";
	private final Request request;

	RequestWrapper(Request request) {
		this.request = request;
		setUpMultipartConfiguration();
	}

	@Override
	protected InvocationHandler handler() {
		return (proxy, method, args) -> mapParameterToMethod(method.getName());
	}

	private Object mapParameterToMethod(String method) {

		try {
			Object value = (method.startsWith("header")) ? headerForName(method) : parameterForName(method);

			if (value instanceof InputStream)
				return value;

			if (value instanceof Request)
				return value;

			RequestAdapter adapter = adapterFor(method);
			return method.endsWith("List") ? adapter.adaptList((String) value) : adapter.adapt((String) value);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			throw new RuntimeException("Error receiving parameters");
		}
	}

	private void setUpMultipartConfiguration() {
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(System.getProperty("java.io.tmpdir"));
		request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
	}

	private RequestAdapter adapterFor(String method) {
		return adapters.requestAdapterOf(method);
	}

	private String headerForName(String method) {
		return request.headers(normalizeHeaderName(method));
	}

	private String normalizeHeaderName(String headerName) {
		char[] header = headerName.replace("header", "").toCharArray();
		StringBuilder result = new StringBuilder();

		for (char character : header) {
			boolean upperCase = Character.isUpperCase(character);
			character = upperCase ? Character.toLowerCase(character) : character;
			String separator = upperCase && result.length() > 0 ? "-" : "";
			result.append(character + separator);
		}

		return result.toString();
	}

	private Object parameterForName(String method) throws IOException, ServletException {
		if (method.equals(SessionId))
			return request.session().id();

		if (method.equals(RequestUrl))
			return requestUrl();

		if (method.equals(LanguageFromUrl))
			return languageFromUrl();

		if (method.equals(LanguageFromHeader))
			return languageFromHeader();

		if (method.equals(Request))
			return request;

		if (isMultipart(method))
			return readPart(method);

		if (request.queryParams(method) == null)
			return request.params(method);

		return request.queryParams(method);
	}

	private String requestUrl() {
		String result = request.raw().getRequestURL().toString();

		String forwardedProto = request.raw().getHeader("X-Forwarded-Proto");
		if (forwardedProto != null && forwardedProto.equals("https"))
			result = result.replace("http:", "https:");

		return result;
	}

	private String languageFromUrl() {
		return languageOf(request.queryParams("language"));
	}

	private String languageFromHeader() {
		String language = request.raw().getHeader("Accept-Language");
		return language != null ? languageOf(language.split(",")[0]) : null;
	}

	private String languageOf(String language) {
		if (language == null) return null;
		return Locale.forLanguageTag(language).toString().replaceAll("_.*", "");
	}

	private boolean isMultipart(String method) throws IOException, ServletException {
		return request.contentType() != null && request.contentType().contains("multipart/form-data") && request.raw().getPart(method) != null;
	}

	private Object readPart(String method) throws IOException, ServletException {
		Part part = request.raw().getPart(method);

		if (part.getContentType() == null)
			return readString(part.getInputStream());

		return part.getInputStream();
	}

	private String readString(InputStream stream) throws IOException {
		Scanner scanner = new Scanner(stream, "UTF-8").useDelimiter("\\A");
		return scanner.hasNext() ? scanner.next() : "";
	}
}
