package cotton.framework.actions;

public interface AdapterProxy {
    <T extends RequestAdapter> T requestAdapterOf(String name);
    <T extends ResponseAdapter> T responseAdapterOf(String name);
}
