package cotton.framework.setups.web.services;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import cotton.framework.core.Client;
import cotton.framework.core.Session;
import cotton.framework.services.BrowserService;

import java.util.*;
import java.util.function.Consumer;

public class PushService implements cotton.framework.services.PushService {

	protected final List<Consumer<Client>> openConnectionListeners = new ArrayList<>();
	private final Map<String, List<Consumer<JsonElement>>> messageListeners = new HashMap<>();
	protected final Map<String, List<Consumer<Client>>> closeConnectionListeners = new HashMap<>();
	protected final List<Client> clients = new ArrayList<>();
	protected final BrowserService service;

	private static final JsonParser PARSER = new JsonParser();
	private static final String Url = "?id=%s&session=%s&language=%s";

	public PushService(BrowserService service) {
		this.service = service;
	}

	@Override
	public String queryString(String clientId, String sessionId, String language) {
		return String.format(Url, clientId, sessionId, language);
	}

	@Override
	public void onOpen(Consumer<Client> consumer) {
		openConnectionListeners.add(consumer);
	}

	@Override
	public Connection onMessage(String clientId, Consumer<JsonElement> consumer) {
		messageListeners.putIfAbsent(clientId, new ArrayList<>());
		messageListeners.get(clientId).add(consumer);
		return () -> { synchronized(messageListeners) { messageListeners.get(clientId).remove(consumer); } };
	}

	@Override
	public ClosedConnection onClose(String clientId) {
		return consumer -> {
			closeConnectionListeners.putIfAbsent(clientId, new ArrayList<>());
			closeConnectionListeners.get(clientId).add(consumer);
		};
	}

	public void onOpen(Client client) {
		service.registerClient(client);
		service.linkToThread(client);

		clients.add(client);
		openConnectionListeners.forEach(listener -> listener.accept(client));
		openConnectionListeners.clear();

		service.unlinkFromThread();
	}

	public void onMessage(Client client, String message) {
		broadcastMessage(client, PARSER.parse(message));
	}

	public void onClose(Client client) {
		service.unRegisterClient(client);
		clients.remove(client);
		messageListeners.remove(client.id());
		closeConnectionListeners.get(client.id()).forEach(clientConsumer -> clientConsumer.accept(client));
		closeConnectionListeners.remove(client.id());
	}

	@Override
	public void pushBroadcast(JsonElement message) {
		clients.stream().forEach(connection -> connection.send(message.toString()));
	}

	@Override
	public void pushToSession(Session session, JsonElement message) {
		session.send(message.toString());
	}

	@Override
	public void pushToClient(Client client, JsonElement message) {
		client.send(message.toString());
	}

	private void broadcastMessage(Client client, JsonElement message) {
		messageListeners.get(client.id()).forEach(listener -> listener.accept(message));
	}
}
