package cotton.framework.services;

import cotton.framework.core.Client;
import cotton.framework.core.Session;

public interface BrowserService extends Service {

	void registerClient(Client client);
	void unRegisterClient(Client client);
	boolean existsClient(String id);
	<T extends Client> T getClient(String id);
	<T extends Client> T currentClient();
	void linkToThread(Client client);
	void unlinkFromThread();

	<T extends Session> T getSession(String id);
	<T extends Session> T currentSession();

}
