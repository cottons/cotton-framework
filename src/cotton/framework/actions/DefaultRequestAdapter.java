package cotton.framework.actions;

import java.util.List;

import static java.util.Collections.emptyList;

public class DefaultRequestAdapter implements RequestAdapter<String> {

    @Override
    public String adapt(String value) {
        return value;
    }

    @Override
    public List<String> adaptList(String value) {
        return emptyList();
    }
}
