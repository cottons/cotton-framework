package cotton.framework.setups.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import cotton.framework.actions.AdapterProxy;
import cotton.framework.actions.ResponseAdapter;
import cotton.framework.core.Client;
import cotton.framework.core.Session;
import cotton.framework.services.PushService;

import java.util.List;
import java.util.Map;

public class WebMessageCarrier implements cotton.framework.displays.MessageCarrier {
	private static final JsonParser parser = new JsonParser();
	private final AdapterProxy adapterProxy;
	private final PushService pushService;

	public WebMessageCarrier(AdapterProxy adapterProxy, PushService pushService) {
		this.adapterProxy = adapterProxy;
		this.pushService = pushService;
	}

	@Override
	public void notifyAll(String message, Map<String, Object> parameters) {
		pushService.pushBroadcast(serializeMessage(message, parameters));
	}

	@Override
	public void notify(Client client, String message, Map<String, Object> parameters) {
		pushService.pushToClient(client, serializeMessage(message, parameters));
	}

	@Override
	public void notify(Session session, String message, Map<String, Object> parameters) {
		pushService.pushToSession(session, serializeMessage(message, parameters));
	}

	private JsonElement serializeMessage(String name, Map<String, Object> parameters) {
		JsonObject result = new JsonObject();
		result.addProperty("name", name);
		if (parameters != null)
			result.add("parameters", serializeMessageParameters(parameters));
		return result;
	}

	private JsonElement serializeMessageParameters(Map<String, Object> parameters) {
		JsonObject result = new JsonObject();
		parameters.entrySet().stream().forEach(e -> {
			result.add(e.getKey(), serializeMessageParameter(e.getKey(), e.getValue()));
		});
		return result;
	}

	private JsonElement serializeMessageParameter(String key, Object value) {
		ResponseAdapter adapter = adapterProxy.responseAdapterOf(key);
		String result = value instanceof List ? adapter.adaptList((List) value) : adapter.adapt(value);

		try {
			return parser.parse(result);
		} catch (Exception exception) {
			return new JsonPrimitive(result);
		}
	}

}
