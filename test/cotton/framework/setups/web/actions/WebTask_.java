package cotton.framework.setups.web.actions;

import cotton.framework.actions.Action;
import cotton.framework.core.Client;
import cotton.framework.services.BrowserService;
import cotton.framework.services.ServiceSupplier;
import cotton.framework.setups.web.actions.common.OutputMessage;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class WebTask_ {

	private ServiceSupplier serviceSupplier;
	private BrowserService browserService;

	@Before
	public void setUp() {
		serviceSupplier = mock(ServiceSupplier.class);
		browserService = mock(BrowserService.class, RETURNS_MOCKS);
		when(serviceSupplier.service(BrowserService.class)).thenReturn(browserService);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void should_link_request_with_session_on_calling_to_before() {
		WebTask task = new TestableWebTask(serviceSupplier);

		task.before(new InputMessage(), mock(Action.Output.class));

		verify(browserService).linkToThread(any(Client.class));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void should_unlink_request_for_current_thread_on_calling_to_after() {
		WebTask task = new TestableWebTask(serviceSupplier);

		task.after(new InputMessage(), any(Action.Output.class));

		verify(browserService).unlinkFromThread();
	}

	private class InputMessage implements WebTask.Input {

		@Override
		public String sessionId() {
			return "anyString";
		}

		@Override
		public String clientId() {
			return "anyString";
		}

		@Override
		public String requestUrl() {
			return "anyString";
		}

		@Override
		public String languageFromUrl() {
			return "en";
		}

		@Override
		public String languageFromHeader() {
			return "en";
		}

		@Override
		public String displayId() {
			return "anyThing";
		}

		@Override
		public String operation() {
			return "anyThing";
		}
	}

	private class TestableWebTask extends WebTask<WebTask.Input, OutputMessage> {

		public TestableWebTask(ServiceSupplier serviceSupplier) {
			super(serviceSupplier);
		}

		@Override
		public void execute(WebTask.Input input, OutputMessage output) {
		}

		@Override
		protected Client clientOf(String id) {
			return mock(Client.class);
		}
	}
}
