package cotton.framework.displays;

import cotton.framework.core.Client;
import cotton.framework.services.BrowserService;
import cotton.framework.services.ServiceSupplier;

import java.util.*;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;

public class Display {
    private final String id;
    private final List<Display> children = new ArrayList<>();
    protected final MessageCarrier carrier;
    protected final DisplayRepository repository;
    protected ServiceSupplier serviceSupplier;

    public Display(MessageCarrier carrier, DisplayRepository repository) {
        this.id = UUID.randomUUID().toString();
        this.carrier = carrier;
        this.repository = repository;
    }

    public final void personify() {
        carry("personify", initializationParameters());
        init();
    }

    public final void personifyOnce() {
        carry("personifyOnce", initializationParameters());
        init();
    }

    protected void init() {
    }

    public void setLanguage(String language) {
        propagateLanguageChanged(language);
    }

    private void propagateLanguageChanged(String language) {
        if (this instanceof International)
            ((International)this).onChangeLanguage(language);

        for (Display child : children)
            child.propagateLanguageChanged(language);
    }

    public void refresh() {
    }

    public String id() {
        return id;
    }

    public void die() {
        carry("die", singletonMap("id", id));
    }

    public void inject(ServiceSupplier serviceSupplier) {
        this.serviceSupplier = serviceSupplier;
    }

    public List<Display> children(Class<? extends Display> clazz) {
        return children.stream().filter(child -> child.getClass().isAssignableFrom(clazz)).collect(toList());
    }

    public <T extends Display> T child(Class<T> clazz) {
        return (T) children(clazz).stream().findFirst().orElse(null);
    }

    public void add(Display child) {
        child.inject(serviceSupplier);
        repository.register(child);
        this.children.add(child);
    }

    public void addAndPersonify(Display child) {
        add(child);
        child.personify();
    }

    public void remove(Class<? extends Display> clazz) {
        List<Display> childrenToRemove = children(clazz);
        for (Display display : childrenToRemove) {
            display.die();
            children.remove(display);
            repository.remove(display.id);
        }
    }

    public String name() {
        return nameOf(this.getClass());
    }

    public static String nameOf(Class<? extends Display> clazz) {
        return clazz.getSimpleName().replace("Display", "").toLowerCase();
    }

    protected void carry(String message) {
        carry(message, emptyMap());
    }

    protected void carry(String message, Map<String, Object> parameters) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, parameters);
    }

    protected void carryWithId(String message, Map<String, Object> parameters) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, addIdTo(parameters));
    }

    protected void carryWithId(String message) {
        carryWithId(message, new HashMap<>());
    }

    protected void carry(String message, Object parameter) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, parameter);
    }

    protected void carryWithId(String message, Object parameter) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, addIdTo(new HashMap<String, Object>() {{ put(message, parameter); }}));
    }

    protected void carry(String message, String parameter, Object value) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, parameter, value);
    }

    protected void carryWithId(String message, String parameter, Object value) {
        Client client = browserService().currentClient();
        carrier.notify(client, message, addIdTo(new HashMap<String, Object>() {{ put(parameter, value); }}));
    }

    private BrowserService browserService() {
        return serviceSupplier.service(BrowserService.class);
    }

    private Map<String, Object> initializationParameters() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("display", name());
        return parameters;
    }

    private Map<String, Object> addIdTo(Map<String, Object> parameters) {
        Map<String, Object> parametersWithId = new HashMap<>();
        parametersWithId.putAll(parameters);
        parametersWithId.put("id", id);
        return parametersWithId;
    }

}
