package cotton.framework.setups;

import java.io.File;
import java.net.URI;
import java.net.URL;

public interface Configuration {
    String appName();
    String title();
    String subtitle();

    URL logoUrl();
    File logo();
    URL resourceUrl(URL resource);
    URL resourceUrl(URL resource, String contentType);

    URI uri();
    int port();

    boolean openInBrowser();
}
