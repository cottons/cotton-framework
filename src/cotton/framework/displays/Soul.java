package cotton.framework.displays;

import java.util.HashMap;
import java.util.Map;

public abstract class Soul implements HistoryBoard, DisplayRepository {
    private final Map<String, Display> displays = new HashMap<>();

    @Override
    public <T extends Display> T get(String id) {
        return (T) displays.get(id);
    }

    @Override
    public <T extends Display> void register(T display) {
        displays.put(display.id(), display);
    }

    @Override
    public void remove(String id) {
        displays.remove(id);
    }
}
