package cotton.framework.services;

import com.google.gson.JsonElement;
import cotton.framework.core.Client;
import cotton.framework.core.Session;

import java.util.function.Consumer;

public interface PushService extends Service {

	String queryString(String sessionId, String clientId, String language);

	void onOpen(Consumer<Client> consumer);
	Connection onMessage(String clientId, Consumer<JsonElement> listener);
	ClosedConnection onClose(String clientId);

	void pushBroadcast(JsonElement message);
	void pushToSession(Session session, JsonElement message);
	void pushToClient(Client client, JsonElement message);

	interface OpenConnectionListener {
		void onOpen(Client client);
	}

	interface MessageListener {
		void onMessage(JsonElement message);
	}

	interface ClosedConnection {
		void execute(Consumer<Client> client);
	}

	interface Connection {
		void unRegister();
	}

}
