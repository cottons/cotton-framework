package cotton.framework.actions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

public class DefaultResponseAdapter implements ResponseAdapter<Object> {

    @Override
    public String adapt(Object value) {

        if (value instanceof String)
            return (String)value;

        if (value instanceof cotton.framework.core.Error)
            return adaptError((cotton.framework.core.Error)value);

        if (value instanceof List)
            return adaptList((List)value);

        return adaptObject(value).toString();
    }

    @Override
    public String adaptList(List<Object> values) {
        return values.stream()
                .map(this::adaptObject)
                .collect(JsonArray::new, JsonArray::add, JsonArray::addAll).toString();
    }

    private JsonElement adaptObject(Object value) {
        return new Gson().toJsonTree(value);
    }

    private String adaptError(cotton.framework.core.Error error) {
        JsonObject object = new JsonObject();

        object.addProperty("code", error.code());
        object.addProperty("label", error.label());
        adaptParameters(object, error.parameters());

        return object.toString();
    }

    private void adaptParameters(JsonObject object, Map<String, String> parameters) {
        for (Map.Entry<String, String> parameter : parameters.entrySet())
            object.addProperty(parameter.getKey(), parameter.getValue());
    }

}
