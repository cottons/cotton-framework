package cotton.framework.setups.web.actions.common;


import cotton.framework.actions.Action;

public interface InputMessage extends Action.Input {
    String sessionId();
    String message();
}
