package cotton.framework.setups.web.actions.common;

import cotton.framework.actions.Action;

public interface OutputMessage extends Action.Output {
    void write(String message);
}
