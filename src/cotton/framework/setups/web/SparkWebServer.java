package cotton.framework.setups.web;

import cotton.framework.actions.Router;
import cotton.framework.setups.CottonServer;
import cotton.framework.setups.web.actions.SparkRouter;

public class SparkWebServer extends CottonServer {
	private final int port;
	private final Router router;

	public SparkWebServer(String appName, int port) {
		super(appName);
		this.port = port;
		this.router = createRouter();
	}

	@Override
	protected Router router() {
		return router;
	}

	private Router createRouter() {
		Router router = new SparkRouter(port, this);
		router.staticFiles("web/");
		return router;
	}
}
