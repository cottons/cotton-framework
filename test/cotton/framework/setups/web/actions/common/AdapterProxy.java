package cotton.framework.setups.web.actions.common;

import cotton.framework.actions.RequestAdapter;
import cotton.framework.actions.ResponseAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterProxy implements cotton.framework.actions.AdapterProxy {

    private final Map<String, RequestAdapter> requestAdapters = new HashMap<>();
    private final Map<String, ResponseAdapter> responseAdapters = new HashMap<>();

    @SuppressWarnings("unchecked")
    @Override
    public RequestAdapter requestAdapterOf(String name) {
        return requestAdapters.getOrDefault(name, defaultRequestAdapter());
    }

    @SuppressWarnings("unchecked")
    @Override
    public ResponseAdapter responseAdapterOf(String name) {
        return responseAdapters.getOrDefault(name, defaultResponseAdapter());
    }

    private RequestAdapter defaultRequestAdapter() {
        return new RequestAdapter<String>() {
            @Override
            public String adapt(String value) {
                return value;
            }

            @Override
            public List<String> adaptList(String value) {
                throw new UnsupportedOperationException("Not used");
            }
        };
    }

    private ResponseAdapter defaultResponseAdapter() {
        return new ResponseAdapter() {
            @Override
            public String adapt(Object value) {
                return value.toString();
            }

            @Override
            public String adaptList(List value) {
                throw new UnsupportedOperationException("Not used");
            }
        };
    }

    public AdapterProxy registerAdapter(String name, RequestAdapter requestAdapter) {
        requestAdapters.put(name, requestAdapter);
        return this;
    }

    public AdapterProxy registerAdapter(String name, ResponseAdapter responseAdapter) {
        responseAdapters.put(name, responseAdapter);
        return this;
    }
}
