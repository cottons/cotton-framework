package cotton.framework.actions;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

@FunctionalInterface
public interface Action {

    Task task();

    @FunctionalInterface
    interface Task<I extends Input, O extends Output> {

        default void before(I input, O output) {
        }

        void execute(I input, O output);

        default void after(I input, O output) {
        }
    }

    interface Message {
    }

    interface Input extends Message {
        String sessionId();
        String clientId();
        String requestUrl();
        String languageFromUrl();
        String languageFromHeader();
    }

    interface Output extends Message {
        void redirect(URL url);
        void write(String content);
        void write(String content, String contentType);
        void write(byte[] content);
        void write(byte[] content, String filename);
        void write(File file);
        void write(InputStream stream);
        void error(cotton.framework.core.Error error);
    }
}
